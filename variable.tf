#Define variables
variable "image_name" {
        description = "Image for container."
        default = "ghost:latest"
}
        
variable "container_name" {
	description = "Name of the container."
        default = "santosh"
}

variable "int_port" {
        description = "Internal port for the container."
        default  = "2368"
}

variable "ext_port" {
        description = "external port for the container."
        default = "80"
}













